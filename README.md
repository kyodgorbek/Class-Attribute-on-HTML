# Class-Attribute-on-HTML
Class Attribute on HTML
<p class="important">For a one-year period from
     November 2010, the Marugame Genichiro-inokoma  
     Museum of Contemporary Art (MIMOCA) will host a 
     cycle of four Hiroshi Sugimoto exhibitions.</p>
  <p>Each will showcase works by the artist
        thematically contextlized under the headings
	"Science," "Architecture," "History" and
	"Religion" so as to present a comprehensive
	panorama of the artist's oeuvre.</p>
  <p class="important admittance">Hours: 10:00 -18:00
    (No admittance after 17:30)</p>
